# Global gitlab-ci files


## Using

```
include:
  - project: 'skyant/ci'
    ref: main
    file:
      - '/path/file.yml'
```


## Packages

### docker
For using in CI for building & deploying containers
